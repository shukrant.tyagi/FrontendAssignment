const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: false}));

app.get('/',(req, res) => {
  res.redirect('/sort');
});

app.get('/sort', (req, res) => {
  res.render('index');
});

app.post('/sort', (req, res) => {
  var items = req.body;
  var data = items.values;
  var data1 = data.split(',');

  var freq = {};
  var flag = true;

  for(let i=0; i<data1.length;i++) {
      var value = parseInt(data1[i]);
      if(isNaN(value)) {
        flag = false;
        console.log("hello");
      }
      if (freq[value]) {
          freq[value]++;
      } else {
          freq[value] = 1;
      }
 }
  var result = [];
  if(flag) {
     var arr = [];
     for(val in freq) {
       arr.push([val,freq[val]]);
     }
     arr.sort(function(a,b) {
       if(b[1] - a[1] == 0) {
         return a[0]-b[0];
       }
       return b[1] - a[1];
     });
     console.log(arr);
     for(let i=0;i<arr.length;i++) {
       result.push(`${arr[i][0]} is ${arr[i][1]} times`);
     }
   }
   else {
     result.push("Please enter correct input");
   }
    res.render('index',{result});
});

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.locals.error = err;
  res.status(err.status);
  res.render('error',err);
});

app.listen(process.env.PORT || 3000, () => console.log('Example app listening on port 3000!'))
